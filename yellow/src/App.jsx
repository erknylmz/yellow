import React, { Component } from 'react';
import './css/App.css';
import Information from './information.json';

export default class App extends Component {
  constructor(){
    super();
    this.state={
    search:null
    };
  }

  searchSpace=(event)=>{
    let keyword = event.target.value;
    this.setState({search:keyword})
  }

  render(){ 
   
    const items = Information.filter((data)=>{
      if(this.state.search == null)
        return data
      else 
      if(data.user_name.toLowerCase().includes(this.state.search.toLowerCase()) 
        || data.id.includes(this.state.search)
        || data.user_phone.includes(this.state.search))
      {
        return data
      }
    }).map(data=>{
      return(
       <div>
        <ul>
          <li className="list">
            <span className="item">{data.id}  </span>
            <span className="item">{data.user_name}  </span>
            <span className="item">{data.user_phone}  </span>
            <span className="item">{data.status}  </span> 
            <span className="item">{data.eta}  </span>
            <span className="item">{data.parcel_id}  </span>
            <span className="item">{data.sender} </span>
            <span className="item">{data.verification_required}  </span>
            <span className="item">{data.location_id}  </span>
            <span className="item">{data.location_name}  </span>
            <span className="item">{data.location_coordinate_latitude}  </span>
            <span className="item">{data.location_coordinate_longitude}  </span>
            <span className="item">{data.location_status_ok}  </span>
            <span className="item">{data.notes}  </span>
            <span className="item">{data.last_updated}  </span> 
            </li>
          </ul>
        </div>
      )
    })
    return (
      <div>
        <input className="input" type="text" placeholder="Enter user name/ID/Phone number"  onChange={(e)=>this.searchSpace(e)} />
        {items}
      </div>
    )
  }
}